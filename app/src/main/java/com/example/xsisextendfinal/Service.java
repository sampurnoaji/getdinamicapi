package com.example.xsisextendfinal;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;

public interface Service {
    @GET
    Call<ModelApi> getData(@Url String url);
}
